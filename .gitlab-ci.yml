# This file is part of duplicity.
#
# Copyright 2022 Nils Tekampe <nils@tekampe.org>,
# Kenneth Loafman <kenneth@loafman.com>,
# Aaron Whitehouse <code@whitehouse.kiwi.nz>,
# Edgar Soldin <https://soldin.de>
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# include:
#     - template: Dependency-Scanning.gitlab-ci.yml

.test-template: &test-template
    stage: test
    rules:
        - if: $CI_COMMIT_MESSAGE =~ /\[skip[ _-]tests?\]/i || $SKIP_TESTS
          when: never
        - changes:
            - bin/duplicity
            - bin/rdiffdir
            - duplicity/**/*
          when: always
        - when: never

variables:
    # cache local items
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
    # Set to non-interactive so no tzdata prompt
    DEBIAN_FRONTEND: "noninteractive"

stages:
    - qual
    - test
    - deploy

default:
    image: ubuntu:18.04
    before_script:
        # Set locale to prevent UTF-8 errors
        - export LANG=C.UTF-8
        - export LC_ALL=C.UTF-8
        - apt-get update
        - apt-get install -y 2to3 build-essential git intltool lftp librsync-dev
          libffi-dev libssl-dev openssl par2 rdiff tzdata python3-pip
        - python3 -m pip install --upgrade pip
        - python3 -m pip install -r requirements.txt -r requirements.dev
    interruptible: true

code_ub18-04:
    <<: *test-template
    stage: qual
    script:
        - apt-get install -y python3.8 python3.8-dev
        - tox -e code

py27_ub18-04:
    <<: *test-template
    stage: test
    script:
        - apt-get install -y python2.7 python2.7-dev
        - tox -e py27
    allow_failure: true

py35_ub16-04:
    <<: *test-template
    stage: test
    image: ubuntu:16.04
    before_script:
        # Set locale to prevent UTF-8 errors
        - export LANG=C.UTF-8
        - export LC_ALL=C.UTF-8
        - apt-get update
        - apt-get install -y build-essential git intltool lftp librsync-dev
          libffi-dev libssl-dev openssl par2 rdiff tzdata
          python3-pip python3.5 python3.5-dev
        - python3 -m pip install --upgrade pip==20.3.4
        - python3 -m pip install -r requirements.txt -r requirements.dev
    script:
        - apt-get install -y python3.5 python3.5-dev
        - tox -e py35
    allow_failure: true

py36_ub18-04:
    <<: *test-template
    stage: test
    script:
        - apt-get install -y python3.6 python3.6-dev
        - tox -e py36

py37_ub18-04:
    <<: *test-template
    stage: test
    script:
        - apt-get install -y python3.7 python3.7-dev
        - tox -e py37

py38_ub18-04:
    <<: *test-template
    stage: test
    script:
        - apt-get install -y python3.8 python3.8-dev
        - tox -e py38

py39_ub20-04:
    <<: *test-template
    stage: test
    image: ubuntu:20.04
    before_script:
        # Set locale to prevent UTF-8 errors
        - export LANG=C.UTF-8
        - export LC_ALL=C.UTF-8
        - apt-get update
        - apt-get install -y build-essential git intltool lftp librsync-dev
          libffi-dev libssl-dev openssl par2 rdiff tzdata python3-pip
        - python3 -m pip install --upgrade pip
        - python3 -m pip install -r requirements.txt -r requirements.dev
    script:
        - apt-get install -y python3.9 python3.9-dev
        - tox -e py39

py310_ub22-04:
    <<: *test-template
    stage: test
    image: ubuntu:22.04
    before_script:
        # Set locale to prevent UTF-8 errors
        - export LANG=C.UTF-8
        - export LC_ALL=C.UTF-8
        - apt-get update
        - apt-get install -y build-essential git intltool lftp librsync-dev
          libffi-dev libssl-dev openssl par2 rdiff tzdata python3-pip
        - python3 -m pip install --upgrade pip
        - python3 -m pip install -r requirements.txt -r requirements.dev
    script:
        - apt-get install -y python3.10 python3.10-dev
        - tox -e py310

build_pip:
    stage: deploy
    when: manual
    image: ubuntu:20.04
    before_script:
        - apt-get update
        - apt-get install -y git python3-pip git intltool
        - python3 -m pip install --upgrade pip
        - python3 -m pip install -r requirements.dev
        - python3 -m pip install twine
    script:
        - ./setup.py sdist --dist-dir=.
    artifacts:
        paths:
            - duplicity-*.tar.gz
        expire_in: 30 days

build_snap:
    stage: deploy
    when: manual
    image: ubuntudesktop/gnome-3-38-2004
    before_script:
        # Set locale to prevent UTF-8 errors
        - export LANG=C.UTF-8
        - export LC_ALL=C.UTF-8
        - export SNAP_ARCH=amd64
        - export SNAPCRAFT_BUILD_INFO=1
        - apt-get update
        - apt-get install -y git python3-pip git intltool squashfs-tools
        - python3 -m pip install --upgrade pip
        - python3 -m pip install -r requirements.txt -r requirements.dev
    script:
        - tools/makesnap
        - tools/installsnap
        - tools/testsnap
    artifacts:
        paths:
            - build/duplicity-*/duplicity_*.snap
            - build/duplicity-*/duplicity_*.txt
        expire_in: 30 days

pages:
    stage: deploy
    when: manual
    image: ubuntu:20.04
    script:
        - VERSION=`./setup.py --version`
        - echo "make docs of ${VERSION}"
        - make docs
        - mv docs/_build/html public
    artifacts:
        paths:
            - public
